Source: manila
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools (>= 123~),
 po-debconf,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx <!nodoc>,
Build-Depends-Indep:
 alembic,
 git,
 python3-bashate,
 python3-cachetools,
 python3-cinderclient,
 python3-coverage,
 python3-ddt <!nodoc>,
 python3-defusedxml,
 python3-eventlet,
 python3-fixtures <!nodoc>,
 python3-glanceclient,
 python3-greenlet,
 python3-hacking,
 python3-iso8601,
 python3-keystoneauth1,
 python3-keystonemiddleware,
 python3-lxml,
 python3-mysqldb,
 python3-netaddr,
 python3-neutronclient,
 python3-novaclient,
 python3-openstackdocstheme <!nodoc>,
 python3-os-api-ref,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db (>= 15.0.0),
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging (>= 14.1.0),
 python3-oslo.middleware,
 python3-oslo.policy (>= 4.4.0),
 python3-oslo.privsep,
 python3-oslo.reports,
 python3-oslo.rootwrap,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.upgradecheck,
 python3-oslo.utils,
 python3-oslotest <!nodoc>,
 python3-osprofiler,
 python3-packaging,
 python3-paramiko,
 python3-paste,
 python3-pastedeploy,
 python3-psycopg2,
 python3-pymysql,
 python3-pyparsing,
 python3-requests,
 python3-requests-mock,
 python3-routes,
 python3-sqlalchemy,
 python3-sqlalchemy-utils (>= 0.38.3),
 python3-stestr <!nocheck>,
 python3-stevedore,
 python3-tenacity,
 python3-testresources <!nocheck>,
 python3-testscenarios <!nocheck>,
 python3-testtools,
 python3-tooz,
 python3-webob,
Build-Conflicts:
 bpython,
 ipython,
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/openstack-team/services/manila
Vcs-Git: https://salsa.debian.org/openstack-team/services/manila.git
Homepage: https://github.com/openstack/manila

Package: manila-api
Architecture: all
Depends:
 adduser,
 debconf,
 manila-common (= ${binary:Version}),
 python3-keystoneclient,
 python3-openstackclient,
 python3-pastescript,
 q-text-as-data,
 uwsgi-plugin-python3,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack shared file system as a service - API server
 Manila is an OpenStack project to provide Shared Filesystems as a service.
 It provides coordinated access to shared or distributed file systems. While
 the primary consumption of file shares would be across OpenStack Compute
 instances, the service is also intended to be accessible as an independent
 capability in line with the modular design established by other OpenStack
 services. Manila is extensible for multiple backends (to support vendor or
 file system specific nuances / capabilities) and accommodates any of a
 variety of shared or distributed file system types.
 .
 This package contains the Manila API server.

Package: manila-common
Architecture: all
Depends:
 adduser,
 dbconfig-common,
 python3-manila (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack shared file system as a service - common files
 Manila is an OpenStack project to provide Shared Filesystems as a service.
 It provides coordinated access to shared or distributed file systems. While
 the primary consumption of file shares would be across OpenStack Compute
 instances, the service is also intended to be accessible as an independent
 capability in line with the modular design established by other OpenStack
 services. Manila is extensible for multiple backends (to support vendor or
 file system specific nuances / capabilities) and accommodates any of a
 variety of shared or distributed file system types.
 .
 This package contains commons components for Manila.

Package: manila-data
Architecture: all
Depends:
 manila-common (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: Manila storage service - Data service
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to implement
 and massively scalable.
 .
 Manila is the OpenStack shared filesystem service.
 .
 This package contains the Manila Data service.

Package: manila-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack shared file system as a service - Doc
 Manila is an OpenStack project to provide Shared Filesystems as a service.
 It provides coordinated access to shared or distributed file systems. While
 the primary consumption of file shares would be across OpenStack Compute
 instances, the service is also intended to be accessible as an independent
 capability in line with the modular design established by other OpenStack
 services. Manila is extensible for multiple backends (to support vendor or
 file system specific nuances / capabilities) and accommodates any of a
 variety of shared or distributed file system types.
 .
 This package contains the documentation.

Package: manila-scheduler
Architecture: all
Depends:
 manila-common (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack shared file system as a service - Scheduler server
 Manila is an OpenStack project to provide Shared Filesystems as a service.
 It provides coordinated access to shared or distributed file systems. While
 the primary consumption of file shares would be across OpenStack Compute
 instances, the service is also intended to be accessible as an independent
 capability in line with the modular design established by other OpenStack
 services. Manila is extensible for multiple backends (to support vendor or
 file system specific nuances / capabilities) and accommodates any of a
 variety of shared or distributed file system types.
 .
 This package contains the Manila Scheduler server.

Package: manila-share
Architecture: all
Depends:
 manila-common (= ${binary:Version}),
 python3-ceph-argparse,
 python3-rados,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack shared file system as a service - Share server
 Manila is an OpenStack project to provide Shared Filesystems as a service.
 It provides coordinated access to shared or distributed file systems. While
 the primary consumption of file shares would be across OpenStack Compute
 instances, the service is also intended to be accessible as an independent
 capability in line with the modular design established by other OpenStack
 services. Manila is extensible for multiple backends (to support vendor or
 file system specific nuances / capabilities) and accommodates any of a
 variety of shared or distributed file system types.
 .
 This package contains the Manila Share daemon.

Package: python3-manila
Section: python
Architecture: all
Depends:
 alembic,
 e2fsprogs,
 python3-cachetools,
 python3-cinderclient,
 python3-defusedxml,
 python3-eventlet,
 python3-glanceclient,
 python3-greenlet,
 python3-keystoneauth1,
 python3-keystonemiddleware,
 python3-lxml,
 python3-netaddr,
 python3-neutronclient,
 python3-novaclient,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db (>= 15.0.0),
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging (>= 14.1.0),
 python3-oslo.middleware,
 python3-oslo.policy (>= 4.4.0),
 python3-oslo.privsep,
 python3-oslo.reports,
 python3-oslo.rootwrap,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.upgradecheck,
 python3-oslo.utils,
 python3-osprofiler,
 python3-packaging,
 python3-paramiko,
 python3-paste,
 python3-pastedeploy,
 python3-pbr,
 python3-psycopg2,
 python3-pymysql,
 python3-pyparsing,
 python3-requests,
 python3-routes,
 python3-sqlalchemy,
 python3-sqlalchemy-utils (>= 0.38.3),
 python3-stevedore,
 python3-tenacity,
 python3-tooz,
 python3-webob,
 ${misc:Depends},
 ${python3:Depends},
Conflicts:
 python-manila,
Description: OpenStack shared file system as a service - Python libs
 Manila is an OpenStack project to provide Shared Filesystems as a service.
 It provides coordinated access to shared or distributed file systems. While
 the primary consumption of file shares would be across OpenStack Compute
 instances, the service is also intended to be accessible as an independent
 capability in line with the modular design established by other OpenStack
 services. Manila is extensible for multiple backends (to support vendor or
 file system specific nuances / capabilities) and accommodates any of a
 variety of shared or distributed file system types.
 .
 This package contains the Python library for Manila.
